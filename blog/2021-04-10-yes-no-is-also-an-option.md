---
author: "Muhammad Ali Kazmi"
tags: ["software crafting"]
keywords: ["software crafting", "soft skills"]
description: "Saying NO is a challenge in our crafting days. Especially when we as software developers find ourselves in situations where we are supposed to make commitments. Why do we have a problem with that? We at Codementors had a small discussion about different reasons behind saying a YES even when its not appropriate. In this blog we will share some of the reasons we experienced."
---

# Yes, No is also an option!

> “The difference between successful people and really successful people is that really successful people say no to almost everything.” [Warren Buffet]

Saying **_NO_** is a challenge in our crafting days. Especially when we as software developers find ourselves in situations where we are supposed to make commitments. Why do we have a problem with that? We at Codementors had a small discussion about different reasons behind saying a "**_yes_**" even when its not appropriate. In this blog we will share some of the reasons we experienced. Each decision has its implications. How to deal with that as a software crafter especially if the decision is not yours!

After this small discussion, we decided to gather the _reasons_ behind such decisions made by software developers. In this part we will concentrate only about situations, where an explicit “**_no_**” is relevant.

## Types of **_YES_**

To make it simple we differentiate between:

- an unsaid no becomes a **_yes_**.
- accepting a **_yes_** after saying **_no_**.
- saying **_yes_** and meaning it.

Let us start with the last one. I will call it a real and committed **_yes_**. The person saying this sort of **_yes_** is motivated and believe in their commitment. I experienced that people accept the responsibility after such commitment even if they fail. And this is the only “**_YES_**” which exists.

## Reason No. 1: **Consequences**

_“I need to do it! Otherwise I am out of the game!”_ Sounds familiar, right?

For example a team developing a system get an invitation from management:

> Manager: "We have to deliver the system with all the functionalities until a specific date."  
> Team: It sounds impossible. We need to cut some of the functionalities.  
> Manager: …  
> Team: …  
> Manager: If your team cannot do it, we will hire a team from another vendor, which can do it.  
> Team (in most cases the account manager): If another team can do it, why couldn’t we?

Sounds like the only option that team has is saying a **_yes_** at this moment. Here we are talking about a **_yes_** after saying a **_no_**. The reason behind this behavior are the **Consequences**: The developers could loose their job, if they does not hit the goal. And they won't because the root cause of their **_no_** will not be discussed or solved.

### How to deal with it?

Developers in team say no because of the concerns they have. I experienced a similar situation years back. We sat together and listed all the risks and concerns we had. We made them transparent to stakeholders including management. We checked weekly, if a new risk should be added to our list and if any of the risk probability has risen. Management received the updated list regularly and took actions to mitigate the risks. We also discuss the scope of system regularly with our product owner. We had new requirements and some of the old requirements became obsolete. Also these changes were brought to managements knowledge regularly. The system was not operational on given date with all functionalities. Management understood the reasons and nobody lost their job. On the contrary the team was appreciated for the way they worked.

## Reason No. 2: **Psychological Safety**

Amy Admondson wrote about a nurse in her book _The Fearless Organization_. Once while assisting a medical doctor, she was pretty sure, that the doctor is making a mistake while treating a patient. She didn’t say anything. **Psychological Safety** was the reason behind her silence.

If employees feel psychological unsafe in a team or organization, they wouldn't say a **_no_** to anything. In this case we talk about "an unsaid **_no_** becomes a **_yes_**". This is a hell for a software crafter. Management/seniors expect that employees/juniors are only there to follow their commands. Because nobody contradict any of their decisions, everybody is committed, according to management/seniors point of view.

After some time employees leave such teams or organizations. The quality of the software becomes worse. Customers become unsatisfied. These are some of the symptoms we noticed in such cases. If nothing get done against this attitude, this becomes the new culture. And that my dear is hell of a job to change! :-)

### How to deal with it?

In such a situation a significant change is needed on both, management's and developers' side. One of the most important instrument a software crafter should have are communication skills and specially the art of giving and accepting feedback. We don't need to attack our seniors or management because of the decisions. Try to understand and figure out the motivation behind such decisions or commands. E.g. the medical doctor in above example is honest about saving his patient.

The nurse could have told the doctor:

1. "Excuse me but what you are doing right now appears like a mistake to me because ..." OR
2. "Thank God, you are here doctor. Otherwise, I could have done _xyz_ instead of what you are doing."

The first option seems difficult in an psychological unsafe environment. The nurse may doesn't want to tell her boss that he is making a mistake.  
In the second option a feedback is given without stepping on anyone's toes. Staying silent forever is not a solution but a time passing strategy. Which is not going to help anybody!

I also believe that software crafters also welcome feedback even if it hurts.

In case of a team, my interpretation for a similar example by Patrick M. Lencioni given in his classic book _The Five Dysfunctions of a Team_ is to get rid of such seniors. This is a decision which should be made by higher management. Make sure you make it transparent. As i said earlier: "Staying silent is not a solution."

## Conclusion

Our **_yes_** without thinking about any _reasons_ to say **_no_** may hurt us in a near future. It doesn't mean to start with **_no_**. But not to start with **_yes_** as well. We experienced that making our reasons and following risks transparent helped us a lot. Sometimes those risks are only in our imagination. Talking to teammates and stakeholders help us figure out the reality. The same is true for a team, where before communicating concerns with management, team members should discuss these thoroughly amongst themselves. It is not easy all the time. It helped us live in a comfortable state of mind after making our concerns transparent. Which, in turn, directly increases productivity.

List some decisions for yourself from last couple of years you regret. To which category they belong? **YES** or **NO**
