# codementors website

## Development

### Blog Authors and Members

Add and edit members of codementors in `blog/authors.json`. The list of authors
will be rendered on the landingpage. Reference the author in the blogposts
according to the
[docusaurus documentation](https://docusaurus.io/docs/blog#global-authors).

```markdown
---
authors: <key of authors.json>
---
```

### Setup

Node.js >= 18.16 is required.

Install the dependencies with

```shell
npm install
```

or

```shell
docker compose run docs npm install
```

Run the development server with

```shell
npm start
```

or

```shell
docker compose up
```

and open http://localhost:3000 in your browser.

#### Commands

Run additional commands with `npm run <command>` from the repository root.

<!-- regenerate with `npm run | pbcopy` -->

```text
Lifecycle scripts included in codementors-website:
  start
    docusaurus start

available via `npm run-script`:
  docusaurus
    docusaurus
  build
    docusaurus build
  swizzle
    docusaurus swizzle
  deploy
    docusaurus deploy
  clear
    docusaurus clear
  serve
    docusaurus serve
  lint:types
    tsc --noEmit
```

#### Editors

##### Jetbrains IDEs

###### Prettier Configuration

Configure the
[Prettier plugin](https://plugins.jetbrains.com/plugin/10456-prettier) to run
'On save' and/or 'On "reformat code" action' to keep the markdown files tidy.

Path: <kbd>Settings</kbd> &gt; <kbd>Languages & Frameworks</kbd> &gt;
<kbd>JavaScript</kbd> &gt; <kbd>Prettier</kbd>

Run for files: `{**/*,*}.{json,yml,yaml,js,ts,jsx,tsx,md,mdx}`

![JetBrains Prettier settings](.readme-assets/jetbrains-prettier-config.png)

##### VS Code

###### Prettier Configuration

Install the
[Prettier extension](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
and activate the `Format on Save` in the settings.

Path: <kbd>File</kbd> &gt; <kbd>Preferences</kbd> &gt; <kbd>Settings</kbd> &gt;
Search `Format On Save`
