---
sidebar_position: 3
---

# Craftsmanship Basics

This is a collection of **opinionated** guidelines and best-practices we
continuously collect and evolve in our daily developer work as well as a result
of fruitful discussions in our [Discord](https://discord.gg/V3FZdkyVEJ). These
guidelines are organized along the development lifecycle of a software
development project.
