---
sidebar_position: 2
---

# Community

codementors is a community of passionated Software Developer. It was founded by
[Sascha Bleidner](https://gitlab.com/TribuneX),
[Daniel Haftstein](https://gitlab.com/dhaftstein) and
[Jens Meinecke](https://gitlab.com/meinjens) on 16th of July in 2019.

## Naming

As we all know: Naming is one of the hardest challenges in writing code. The
same applies for the name of this community. We
[worked for hours](https://gitlab.com/codementors/docs/-/blob/master/naming.adoc)
to come up with a good name for this community.

Finally, we settled on the name **codementors**. Its easy to remember and
includes our main goals for this community: **coding** and **mentoring**. That
is what we want to do: Educating people to become better programmers and
ultimately software craftspeople.

:::tip

**Naming convention:** codementors is always written in lower case letters.

:::

## Chat

We use [codementors Discord](https://discord.gg/V3FZdkyVEJ) to share information
and communicate with each other. We try to preserve relevant information in this
handbook to make it easier to find and reuse this information.

## MeetUps

We irregularly meet for Meetups (mostly online) to share knowledge and to train
our software craftspeople skills.

## Stammtisch

A place to meet for networking or discussion with other codementors is the
"codementors Stammtisch". We meet every second week for around one hour on
Wednesday at 20:00 CET. If you would like to take part, take a look at the
[codementors calendar](/docs/Community#calendar) or join our
[codementors Discord](https://discord.gg/V3FZdkyVEJ). The meeting takes place
online. In irregular intervals we may meet in person.

## The Readers Club

We read and study books together. We meet every week for one hour on Wednesday
at 20:00 CET (see: [codementors calendar](/docs/Community#calendar)). If you are
interested, please contact us in
[codementors Discord](https://discord.gg/V3FZdkyVEJ).

### Current Books

The Readers Club is currently paused.

### Previous Books

- November 2023 - March 2024
  [Domain Modelling Made Functional](https://learning.oreilly.com/library/view/domain-modeling-made/9781680505481/)
  by Scott Wlaschin
- Mai 2023 - November 2023
  [Software Architecture: The Hard Parts](https://www.oreilly.com/library/view/software-architecture-the/9781492086888/)
  by Neal Ford, Mark Richards, Pramod Sadalage, Zhamak Dehghani
- January 2023 - Mai 2023:
  [Seven Languages in Seven Weeks](https://www.oreilly.com/library/view/seven-languages-in/9781680500059/)
  by Bruce A. Tate
- Mid 2022 - November 2022:
  [Clean Craftsmanship](https://www.oreilly.com/library/view/clean-craftsmanship-disciplines/9780136915805/)
  by Robert C. Martin
- January 2022 - Mid 2022:
  [Patterns, Principles, and Practices of Domain-Driven Design](https://www.oreilly.com/library/view/patterns-principles-and/9781118714706/)
  by Scott Millett and Nick Tune

## Calendar

We publish the time and location of all our events in this
[codementors calendar](https://calendar.google.com/calendar/ical/ei12k3mdfee6k5inbngfkj86c4%40group.calendar.google.com/private-05fe55bf29b7aa070609e17c86aa07d7/basic.ics)

## History

One day we may look back and ask ourselves: How did it come to this?

| Date       | Milestone         | History                                                                                                                                              |
| ---------- | ----------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- |
| 16/07/2019 | Founding          | First [Meeting](https://gitlab.com/codementors/docs/-/blob/master/meetings/2019-07-16_Meeting_0.jpg) with the title: `Werkstattgespräch "Dev Gilde"` |
| 13/08/2019 | Name is fixed     | We finally agreed on a name: codementors.de was registered as a domain.                                                                              |
| 06/09/2019 | First merchandise | First codementors T-Shirts ordered                                                                                                                   |
| 13/10/2020 | KotlinCodeCamp    | First weekly Kotlin learning event launched                                                                                                          |
| 10/01/2022 | TheReadersClub    | codementors started with the new format: [TheReadersClub](/docs/Community#the-readers-club)                                                          |
