---
slug: /handbook
sidebar_position: 1
---

## Introduction

The codementors handbook is the central repository for how we run this
community. The handbook is inspired by the great
[Gitlab handbook](https://about.gitlab.com/handbook/).

As part of our value of being transparent the handbook is open to the world, and
we welcome feedback. Currently this website is build from a private repository.
We will make this repository public soon, so that you can open a Merge Request
to propose a change or open issus to ask questions.

## Contribution

It is vital to keep this handbook up-to-date with all relevant information
gathered and discussed in our community. Therefore, we want to make it easy to
add, remove or change content within this handbook. Just follow the following
steps to propose a change:

1. Clone this repository
2. Setup your development environment (see:
   [editor configuration](https://gitlab.com/codementors/website/-/blob/master/README.md#editors))
3. Commit your changes
4. Open an MR in the
   [handbook repository](https://gitlab.com/codementors/website/-/merge_requests).
   This marks the start of a discussion for a proposed change in the handbook.
   We do not specifically use the `Draft` prefix function of Gitlab to express
   that an MR is not yet ready to be discussed. Instead, we wait for the label
   `review` to be assigned.
5. Once you feel confident to discuss your proposed change with other
   codementors, add the label `review` to your MR and create a new comment (e.g.
   review please). The label is required to automatically trigger a notification
   in the
   [Discord channel #whats-new](https://discord.com/channels/1069197700958011392/1221003357322346516)
   for your comment.
6. A maintainer of the handbook will assign himself as a reviewer of your MR.
7. Once all discussion threads are resolved, the reviewer will approve and merge
   your MR into the main branch.
