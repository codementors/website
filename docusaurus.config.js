// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const { themes } = require("prism-react-renderer");

const url = process.env.URL || "http://localhost:3000";
const baseUrl = process.env.BASE_URL || "/";

function withBaseUrl(link) {
  return `${baseUrl}${link}`;
}

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "codementors",
  tagline: "cod are cool",
  url,
  baseUrl,
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "favicon.ico",

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: "codementors", // Usually your GitHub org/user name.
  projectName: "website", // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: "de",
    locales: ["de"],
  },

  presets: [
    [
      "classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            "https://gitlab.com/-/ide/project/codementors/website/edit/master/-/",
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            "https://gitlab.com/-/ide/project/codementors/website/edit/master/-/",
          authorsMapPath: "authors.json",
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: "codementors",
        logo: {
          alt: "codementors Logo",
          src: "images/logo/code-mentors-logo.svg",
        },
        items: [
          {
            to: withBaseUrl("docs/handbook"),
            label: "handbook",
            position: "left",
          },
        ],
      },
      footer: {
        style: "dark",
        links: [
          {
            title: "codementors",
            items: [
              {
                to: withBaseUrl("docs/handbook"),
                label: "handbook",
                position: "left",
              },
            ],
          },
          {
            title: "Community",
            items: [
              {
                label: "Slack",
                href: "https://join.slack.com/t/codementorsde/shared_invite/zt-1g3oy3idz-rHOhoYhsaYQWyvBVK9addA",
              },
            ],
          },
          {
            title: "More",
            items: [
              {
                label: "Blog",
                to: withBaseUrl("blog"),
              },
              {
                label: "GitLab",
                href: "https://gitlab.com/codementors",
              },
              {
                label: "Legal notice",
                to: withBaseUrl("legal-notice"),
              },
              {
                label: "Privacy",
                to: withBaseUrl("privacy"),
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} codementors. Built with Docusaurus.`,
      },
      prism: {
        theme: themes.github,
        darkTheme: themes.dracula,
        additionalLanguages: ["bash", "diff", "json"],
      },
    }),

  markdown: {
    mermaid: true,
  },

  themes: [
    /** @docs https://github.com/easyops-cn/docusaurus-search-local */
    [
      require.resolve("@easyops-cn/docusaurus-search-local"),
      {
        hashed: true,
      },
    ],
    "@docusaurus/theme-mermaid",
  ],
};

module.exports = config;
