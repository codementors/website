# Datenschutz

Wir legen großen Wert auf den Schutz deiner Daten. Daher betreiben wir diese
Webseite nach dem Prinzip der Datensparsamkeit. Das bedeutet:

- diese Seite nutzt keine Cookies
- wir benutzen keine Tracking-Tools (z.B. Google-Analytics)
- diese Seite benötigt keine externen Resourcen (z.B. Webfonts)
- deine IP-Adresse wird nicht im Webserver-Log gespeichert

Solltest du uns jedoch eine E-Mail schicken (und das hoffen wir doch!), erklärst
du dich natürlich damit einverstanden, dass folgende personenbezogenen Daten von
dir bei uns gespeichert werden:

- deine E-Mail Adresse
- deine IP-Adresse des Absender Hosts
- deine IP-Adresse des Mailservers

Diese Daten geben wir selbstverständlich an keine Dritten weiter.
