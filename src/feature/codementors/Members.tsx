import * as React from "react";
import styles from "./Members.module.css";
import authors from "../../../blog/authors.json";
import useBaseUrl from "@docusaurus/useBaseUrl";

interface MemberProps {
  name: string;
  title: string;
  url: string;
  imageUrl: string;
}

const Member = ({ name, title, url, imageUrl }: MemberProps) => {
  const withBaseUrl = useBaseUrl(imageUrl);
  const resolvedImageUrl = imageUrl.startsWith("http") ? imageUrl : withBaseUrl;
  return (
    <a href={url} target="_blank" className={styles.member}>
      <img src={resolvedImageUrl} alt={name} className={styles.member__image} />
      <div className={styles.member__details}>
        <span className={styles.member__name}>{name}</span>
        <span className={styles.member__title}>{title}</span>
      </div>
    </a>
  );
};

interface AuthorProps {
  name: string;
  image_url: string;
  url: string;
  title?: string;
}

export const Members = () => {
  const members: AuthorProps[] = Object.values(authors);
  return (
    <div className={styles.members}>
      {members.map(({ name, image_url: imageUrl, url, title }) => (
        <Member
          key={name}
          name={name}
          title={title}
          url={url}
          imageUrl={imageUrl}
        />
      ))}
    </div>
  );
};
